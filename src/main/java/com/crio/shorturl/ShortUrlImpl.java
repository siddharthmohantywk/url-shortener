package com.crio.shorturl;
import java.util.*;
//registerNewUrl(String longUrl) :
//getUrl(String shortUrl)
//delete(String longUrl)
//registerNewUrl(String longUrl)

public class ShortUrlImpl {

    HashMap<String, String> longUrltoShortUrlMap = new HashMap<>();
    HashMap<String, String> shortUrltoLongUrlMap = new HashMap<>();
    HashMap<String, Integer> longUrlHitCountMap = new HashMap<>();

    public String registerNewUrl(String longUrl) {
        //To get the corresponding shortUrl to the given longUrl

        if(longUrltoShortUrlMap.containsKey(longUrl))
        {
            return longUrltoShortUrlMap.get(longUrl);
        }
    
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"+"0123456789"+"abcdefghijklmnopqrstuvxyz";
        StringBuilder shortUrlStringBuilderObject = new StringBuilder(9); //Length of the string
        for (int i = 0; i < 9; i++) {
            //Creates a random 9-character alphanumeric string
            int index = (int)(AlphaNumericString.length() * Math.random()); 
            shortUrlStringBuilderObject.append(AlphaNumericString.charAt(index)); 
        } 

        String shortUrl = "http://short.url/"+shortUrlStringBuilderObject.toString(); 
        longUrltoShortUrlMap.put(longUrl, shortUrl);
        shortUrltoLongUrlMap.put(shortUrl, longUrl);
        longUrlHitCountMap.put(longUrl, 0);
        return shortUrl;
    }

    public String registerNewUrl(String longUrl, String shortUrl){
        if(shortUrltoLongUrlMap.containsKey(shortUrl)){
            return null;
        }

        longUrltoShortUrlMap.put(longUrl, shortUrl);
        shortUrltoLongUrlMap.put(shortUrl, longUrl);
        if(!longUrlHitCountMap.containsKey(longUrl))
        {
            longUrlHitCountMap.put(longUrl, 0);
        }
        return shortUrl;
    }

    public String getUrl(String shortUrl){
        
        if(shortUrltoLongUrlMap.containsKey(shortUrl))
        {
            String longUrlFound = shortUrltoLongUrlMap.get(shortUrl);
            if(longUrlHitCountMap.containsKey(longUrlFound)) {
                int longUrlHitCount = longUrlHitCountMap.get(longUrlFound);
                ++longUrlHitCount;
                longUrlHitCountMap.replace(longUrlFound, longUrlHitCount);
            }
            return shortUrltoLongUrlMap.get(shortUrl);
        }

        return null;
    }

    public void delete(String longUrl)
    {
        if(longUrltoShortUrlMap.containsKey(longUrl))
        {
            shortUrltoLongUrlMap.remove(longUrltoShortUrlMap.get(longUrl));
            longUrltoShortUrlMap.remove(longUrl);
        }
    }

    public Integer getHitCount(String longUrl)
    {
        if(longUrltoShortUrlMap.containsKey(longUrl)){
        return longUrlHitCountMap.get(longUrl);}
        return 0;
    }

}